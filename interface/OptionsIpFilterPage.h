// 北京地铁文件管理系统 - a Windows ftp server

// Copyright (C) 2002-2004 - Tim Kosse <tim.kosse@gmx.de>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#if !defined(AFX_OPTIONSIPFILTERPAGE_H__F3823923_0389_411A_A3F2_516F04AC4665__INCLUDED_)
#define AFX_OPTIONSIPFILTERPAGE_H__F3823923_0389_411A_A3F2_516F04AC4665__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsIpFilterPage.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld COptionsIpFilterPage 

class COptionsDlg;
class COptionsIpFilterPage : public COptionsPage
{
// Konstruktion
public:
	COptionsIpFilterPage(COptionsDlg *pOptionsDlg, CWnd* pParent = NULL);   // Standardkonstruktor

	virtual BOOL IsDataValid();
	virtual void SaveData();
	virtual void LoadData();
	
	// Dialogfelddaten
	//{{AFX_DATA(COptionsIpFilterPage)
	enum { IDD = IDD_OPTIONS_IPFILTER };
	CString	m_AllowedAddresses;
	CString	m_DisallowedAddresses;
	//}}AFX_DATA

// 躡erschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions黚erschreibungen
	//{{AFX_VIRTUAL(COptionsIpFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst黷zung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(COptionsIpFilterPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f黦t unmittelbar vor der vorhergehenden Zeile zus鋞zliche Deklarationen ein.

#endif // AFX_OPTIONSIPFILTERPAGE_H__F3823923_0389_411A_A3F2_516F04AC4665__INCLUDED_
